# sample client - COIN APIs

In this directory, you will find a sample client retrieving subscriber 
information using a fictive COIN API.

To invoke the api, you will have to:

- calculate the sha256 hash of your request body.
- pass a JWT token with the exp and nbf claims as the cookie 'jwt', signed by your RSA private key
- generate a HMAC signature on the digest header, x-date header and the http request line, using a shared secret

The shared key will be generated by COIN and given to use encrypted with your public key. You will need
to decrypt it with your private key.

## JWT token
The JWT token is signed by a RSA private key pair and should contain both an 
expiration (exp) and a not-before (nbf) claim. 

The expiration time should be set to a period of a few minutes in the future.
The not-before date should be set to 30 or 60 seconds before the actual call 
to counter any clock skew.

the issuer should be set to the name of the consumer.

The JWT token is passed using the Authorization header as a bearer token.

## HMAC digest
The HMAC SHA26 digest is calculated over the digest-, x-date header and the HTTP request-line.

the date header should be in the rfc1123 format and the HTTP request line without
the protocol and authority part of the URL.

The HMAC digest is passed using the Authorization header.

REMINDER: The HMAC Secret you receive is encrypted with the public key you provided. In order for you
to use this HMAC string you need to decrypt it with the correlating private key.

## running the sample

to run the example, we have the following prerequisites:

- your are running Linux or MacOS
- you have installed docker-compose (>1.19), docker-ce, maven, make, java, jq and curl
- For the Python client python 3 is required

If you are running windows you require at a minimum the following packages installed to generate ssh keys that will work:

- [git bash](https://git-scm.com/downloads)

## how to generate your private and public key?

In order to generate your own private key, type:

```
ssh-keygen -m PEM -t rsa -b 4096 -f private-key.pem -N '' 
ssh-keygen -e -m PKCS8 -f private-key.pem > public-key.pem
```
never disclose your private key to anyone. The public key can be freely distributed.

### starting the sample service
type:
```
docker-compose up -f src/test/kong/docker-compose.yaml -d
```

## obtaining the shared key
go back to the main repository directory and fetch the sharedkey.encrypted

type:
```
curl -sS http://localhost:8001/consumers/sample-user/hmac-auth | \
	jq -r .data[0].secret | \
	tr -d '\n' | \
	openssl rsautl -encrypt -pubin -inkey public-key.pem | \
	base64 | \
	tr -d '\n' > \
	sharedkey.encrypted
```

If you ever need the shared key in an unencrypted format you can use the following commands:

type:
```
echo sharedkey.encrypted | base64 -d > sharedkey.unbase64
openssl rsautl -decrypt -inkey private-key.pem -in sharedkey.unbase64 -out key.bin
```
the key.bin file now contains the unencrypted shared secret

## update the jwt public key

```
JWT_KEY_ID="$(curl -sS -X GET http://localhost:8001/consumers/sample-user/jwt  | jq -r '.data[0].id')"
DATA="$(jq -n --arg key "$(<public-key.pem)" '{"rsa_public_key": $key}')"  
curl -sS -X PATCH http://localhost:8001/consumers/sample-user/jwt/$JWT_KEY_ID --data "$DATA" -H 'Content-Type: application/json'
```

### running the client
type:
```
make
java -jar target/sample-client.jar
```

the output should look something this:

```
date: Tue, 12 Sep 2017 15:20:42 GMT
GET /subscribers/v1/10000001 HTTP/1.1
lrIFmAzR05ZgrriAQvMz8q2MrxS5txNvjNnV2DKI/4g=
hmac username="sample-user", algorithm="hmac-sha256", headers="date request-line", signature="lrIFmAzR05ZgrriAQvMz8q2MrxS5txNvjNnV2DKI/4g="
service returned 200
{
  "City": "Gouda",
  "FirstName": "Klaas",
  "HouseNumber": "10",
  "Id": 2005,
  "LastName": "Vaak",
  "PhoneNumber": "10000001",
  "Postcode": "2803PK",
  "Prefix": "van",
  "ServiceProvider": "KPN",
  "Street": "Tielweg"
}

date: Tue, 12 Sep 2017 15:20:42 GMT
GET /subscribers/v1/10000001 HTTP/1.1
lrIFmAzR05ZgrriAQvMz8q2MrxS5txNvjNnV2DKI/4g=
hmac username="sample-user", algorithm="hmac-sha256", headers="date request-line", signature="lrIFmAzR05ZgrriAQvMz8q2MrxS5txNvjNnV2DKI/4g="
service returned 401
{"exp":"token expired"}
```

### Running the Python Client
type:
```
python3 pip install -r src/main/python/requirements.txt
```
Run the python call via the following command:
```
python3 src/main/python/rest_subscriber_client.py --consumer-name sample-user --hostname http://localhost:8000/ 10000001
```

