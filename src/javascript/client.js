// This file only contains the required functions to generate the headers for a secure request.

const jsrsasign = require('jsrsasign').jsrsasign;

let consumerName = null;
let privateKey = null;
let encryptedHmacSecretHex = null;

const authorize = function(name, privateKeyAsString, encryptedHmacSecret) {
  consumerName = name;
  privateKey = jsrsasign.KEYUTIL.getKey(privateKeyAsString);
  encryptedHmacSecretHex = Buffer.from(encryptedHmacSecret, 'base64').toString('hex')
};

const generateHmacMessage = function(headers, username, key, requestLine) {
  const mac = new jsrsasign.KJUR.crypto.Mac({ alg: 'HmacSHA256', pass: key });
  const message = Object.keys(headers).map(function (key) { return key + ": " + headers[key] }).join('\n') + '\n' + requestLine;
  mac.updateString(message);
  const hex = mac.doFinal();
  const signature = Buffer.from(hex, 'hex').toString('base64');
  const headersString = Object.keys(headers).join(' ');
  return "hmac username=\"" + username + "\", algorithm=\"hmac-sha256\", headers=\"" + headersString + " request-line\", signature=\"" + signature + "\""
};

const generateHmac = function(method, path, date, additionalHeaders) {
  const headers = { 'x-date': date };
  const sharedKey = jsrsasign.KJUR.crypto.Cipher.decrypt(encryptedHmacSecretHex, privateKey, 'RSA');
  return generateHmacMessage(Object.assign(headers, additionalHeaders), consumerName, sharedKey, method + " " + path + " HTTP/1.1")
};

const generateDigest = function(body) {
  const mac = new jsrsasign.KJUR.crypto.MessageDigest({ alg: 'sha256', prov: 'cryptojs' });
  const hash = mac.digestString(body && JSON.stringify(body));
  const digest = Buffer.from(hash, 'hex').toString('base64');
  return "SHA-256=" + digest
};

const generateToken = function() {
  const oHeader = { alg: 'RS256', typ: 'JWT' };
  const oPayload = {
    iss: consumerName,
    nbf: parseInt((Date.now() / 1000).toString(), 0),
    exp: parseInt((Date.now() / 1000).toString(), 0) + 180
  };
  return jsrsasign.KJUR.jws.JWS.sign('RS256', JSON.stringify(oHeader), JSON.stringify(oPayload), privateKey)
};

const generateHeaders = function(req) {
  const date = (new Date()).toUTCString();
  const path = "/" + req.url.split('/').slice(3).join('/').split('?')[0];
  const digest = generateDigest(req.body || '');
  const signature = generateHmac(req.method, path, date, { digest: digest });
  const jwtToken = generateToken();
  req.headers.cookie = "jwt=" + jwtToken + " path=" + path;
  return {
    Authorization: signature,
    digest: digest,
    'x-date': date,
  }
};
