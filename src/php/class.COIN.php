<?php

class COIN
{
    public static function generateRequestHeaders($userName, $secret, $url, $data = null, $contentType = null, $Body = null)
    {
        $signatureHeaders = array();

		$requestMethod = $contentType;
		
        // Build request-line
        $parsedUrl = parse_url($url);
        $targetUrl = $parsedUrl["path"];
        if (!empty($parsedUrl["query"])) {
            $targetUrl = $targetUrl . "?" . $parsedUrl["query"];
        }
        $requestLine = $requestMethod . " " . $targetUrl . " HTTP/1.1";
		
		// Build Data header
        $dateHeader = COIN::createDateHeader();
		
		// Build Digest header
		$DigestHeader = "SHA-256=".COIN::sha256Base64($Body);
		
		// Prepaire headers for signature
        $signatureHeaders["x-date"] = $dateHeader;
        $signatureHeaders["request-line"] = $requestLine;
		$signatureHeaders["digest"] = $DigestHeader;
		
        // Format the authorization header
        $headers = COIN::getHeadersString($signatureHeaders);
        $signatureString = COIN::getSignatureString($signatureHeaders);
        $signatureHash = COIN::sha256HashBase64($signatureString, $secret);
        $algorithm = "hmac-sha256";
	
        $authHeaderTemplate = 'hmac username="%s",algorithm="%s",headers="%s",signature="%s"';
        $authHeader = sprintf($authHeaderTemplate, $userName, $algorithm, $headers, $signatureHash);
		
		$requestHeaders = array(
                "host" => $parsedUrl["host"],
                "authorization" => $authHeader,
                "x-date" => $dateHeader,
				"digest" => $DigestHeader
            );
		
        return $requestHeaders;
    }
    private static function createDateHeader()
    {
        return gmdate("D, d M Y H:i:s", time()) . " GMT";
    }
    private static function getHeadersString($signatureHeaders)
    {
        $headers = "";
        foreach($signatureHeaders as $key => $val)
        {
            if ($headers !== "") {
                $headers .= " ";
            }
            $headers .= $key;
        }
        return $headers;
    }
    private static function getSignatureString($signatureHeaders)
    {
        $sigString = "";
        foreach($signatureHeaders as $key => $val)
        {
            if ($sigString !== "") {
                $sigString .= "\n";
            }
            if (mb_strtolower($key) === "request-line") {
                $sigString .= $val;
            } else {
                $sigString .= mb_strtolower($key) . ": " . $val;
            }
        }
        return $sigString;
    }
    public static function sha256HashBase64($signatureString, $secret)
    {
        $h = hash_hmac('sha256', $signatureString, $secret, $raw_output = true);
        return base64_encode($h);
    }
	public static function sha256Base64($Body)
    {
        $h = hash('sha256', $Body, $raw_output = true);
        return base64_encode($h);
    }
	private static function jwtverify($signature, $input, $secret, $algo)
    {
        switch ($algo) {
            case'HS256':
            case'HS384':
            case'HS512':
                return COIN::jwtsign($input, $secret, $algo) === $signature;

            case 'RS256':
                return (boolean) openssl_verify($input, $signature, $secret, OPENSSL_ALGO_SHA256);

            case 'RS384':
                return (boolean) openssl_verify($input, $signature, $secret, OPENSSL_ALGO_SHA384);

            case 'RS512':
                return (boolean) openssl_verify($input, $signature, $secret, OPENSSL_ALGO_SHA512);

            default:
                throw new Exception("Unsupported or invalid signing algorithm.");
        }
    }

    private static function jwtsign($input, $secret, $algo)
    {
        switch ($algo) {

            case 'HS256':
                return hash_hmac('sha256', $input, $secret, true);

            case 'HS384':
                return hash_hmac('sha384', $input, $secret, true);

            case 'HS512':
                return hash_hmac('sha512', $input, $secret, true);

            case 'RS256':
                return COIN::jwtgenerateRSA($input, $secret, OPENSSL_ALGO_SHA256);
            case 'RS384':
                return COIN::jwtgenerateRSA($input, $secret, OPENSSL_ALGO_SHA384);
            case 'RS512':
                return COIN::jwtgenerateRSA($input, $secret, OPENSSL_ALGO_SHA512);

            default:
                throw new Exception("Unsupported or invalid signing algorithm.");
        }
    }

    private static function jwtgenerateRSA($input, $secret, $algo)
    {
        if (!openssl_sign($input, $signature, $secret, $algo)) {
            throw new Exception("Unable to sign data.");
        }

        return $signature;
    }

    private static function urlSafeB64Encode($data)
    {
        $b64 = base64_encode($data);
        $b64 = str_replace(array('+', '/', '\r', '\n', '='),
                array('-', '_'),
                $b64);

        return $b64;
    }

    private static function urlSafeB64Decode($b64)
    {
        $b64 = str_replace(array('-', '_'),
                array('+', '/'),
                $b64);

        return base64_decode($b64);
    }

    public static function jwtencode($payload, $secret, $algo = 'HS256')
    {
        $header = array('typ' => 'JWT', 'alg' => $algo);

        $segments = array(
            COIN::urlsafeB64Encode(json_encode($header)),
            COIN::urlsafeB64Encode(json_encode($payload))
        );

        $signing_input = implode('.', $segments);

        $signature = COIN::jwtsign($signing_input, $secret, $algo);
        $segments[] = COIN::urlsafeB64Encode($signature);

        return implode('.', $segments);
    }

    public static function jwtdecode($jwt, $secret = null, $algo = null)
    {
        $tks = explode('.', $jwt);

        if (count($tks) != 3) {
            throw new Exception('Wrong number of segments');
        }

        list($headb64, $payloadb64, $cryptob64) = $tks;

        if (null === ($header = json_decode(COIN::urlsafeB64Decode($headb64)))) {
            throw new Exception('Invalid segment encoding');
        }

        if (null === $payload = json_decode(COIN::urlsafeB64Decode($payloadb64))) {
            throw new Exception('Invalid segment encoding');
        }

        $sig = COIN::urlsafeB64Decode($cryptob64);

        if (isset($secret)) {

            if (empty($header->alg)) {
                throw new DomainException('Empty algorithm');
            }

            if (!COIN::jwtverify($sig, "$headb64.$payloadb64", $secret, $algo)) {
                throw new UnexpectedValueException('Signature verification failed');
            }
        }

        return $payload;
    }
	
	public static function restapi($private_key,$url,$type='GET',$data_string=NULL,$debug=false)
	{
		// HMAC
		$headers = COIN::generateRequestHeaders(RESTCONSUMER, HMAC_SECRET, $url, $data_string, $type, $data_string);
		$headers_array = array();
		foreach($headers as $key=>$value)
		{
			$headers_array[] = $key . ': ' . $value;
		}
		
		//print_r($headers_array); exit;

		// JWT
		$payload = array(
				'iss' => RESTCONSUMER,
				'exp' => time()+120,
				'nbf' => time()-30,
		);

		$token = COIN::jwtencode($payload, $private_key,'RS256');
		$headers_array[] = "Cookie: jwt=$token; path=/;domain=".RESTDOMAIN;
		
		// CURL
		if($debug) ob_start();
		if($debug) $out = fopen('php://output', 'w');	
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		if($debug) curl_setopt($ch, CURLOPT_STDERR, $out);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
		
		if($type=='POST' || $type=='PUT')
		{
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			$headers_array[] = "Content-Type: application/json";
			$headers_array[] = 'Content-Length: ' . strlen($data_string );
		}
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "MaxiTEL-Client");
		$options = array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTPHEADER => $headers_array,
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);


		if($debug) fclose($out);
		if($debug) $data = ob_get_clean();
		if($debug) $data .= PHP_EOL . $result . PHP_EOL;
		if($debug) echo $data;
		
		$decode_api = json_decode($result);
		$errorcode = 0;
			
		if($httpcode!=200)
		{
			if(isset($decode_api->errors[0]->code))
			{
				$errorcode = $decode_api->errors[0]->code;
			}
			else
			{
				$errorcode = $httpcode;
			}
			
			$resultcode = $httpcode;
		}
		elseif(isset($decode_api->errors))
		{
			$errorcode = $decode_api->errors[0]->code;
			$resultcode = $decode_api->errors[0]->code;
		}
		else
		{
			$resultcode = 'OK';
		}

		return array('result'=>$resultcode, 'error_code'=>$errorcode, 'api_result'=>$decode_api, 'api_request'=>$data_string);
		
		/*
		$return = array('code'=>$httpcode, 'result'=>$result);
		// Result
		return $return;
		*/
	}

}